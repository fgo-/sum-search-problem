# -*- coding: utf-8 -*-
"""Methods to search elemnts that add up to a specific target."""
import random
from itertools import combinations


def brute_force_search(nums, target):
    """Return the indices of two distinct elements  that sum to a target.

    If no such elements are found, return `None`.

    This function uses a brute-force approach (complexity is O(N^2)).

    Parameters
    ----------
    nums : list of int
        Candidate elements.
    target : int
        The target.

    Returns
    -------
    (int, int) or None
    """
    # idx_0 != idx_1 is enforced bu using iertools's combinations
    for idx_0, idx_1 in combinations(range(len(nums)), 2):
        if nums[idx_0] + nums[idx_1] == target:
            # assuming there is a single solution, we may exit now
            return (idx_0, idx_1)
    return None


def smarter_search_sort(nums, target):
    """Return the indices of two distinct elements that sum to a target.

    If no such elements are found, return `None`.

    This function first sort the list and then use a linear search (complexity
    is O(N*logN)).

    Parameters
    ----------
    nums : list of int
        Candidate elements.
    target : int
        The target.

    Returns
    -------
    (int, int) or None
    """
    # Sort first (in fact: get the indices that would sort ``nums``)
    # Complexity: N*logN
    idx_sorted = sorted(range(len(nums)), key=nums.__getitem__)
    idx_l = 0  # index of the lower element
    idx_u = len(idx_sorted) - 1  # index of the greater element
    # Browse the sorted list from both ends:
    # Complexity: N
    # nums[idx_sorted[idx_l]] will increase
    # nums[idx_sorted[idx_u]] will decrease
    while idx_l < idx_u:
        sum_ = nums[idx_sorted[idx_l]] + nums[idx_sorted[idx_u]]
        if sum_ == target:
            return (idx_sorted[idx_l], idx_sorted[idx_u])
        elif sum_ < target:
            idx_l += 1
        else:
            idx_u -= 1
    return None


def check_search_result(idx_0, idx_1, nums, target):
    """Check a solution to the sum search problem against the rules.

    Parameters
    ----------
    idx_0 : int
        First solution index.
    idx_1 : int
        Second solution index.
    nums : list of int
        Candidate elements.
    target : int
        The target.
    """
    assert idx_0 != idx_1, 'Same index'
    assert nums[idx_0] + nums[idx_1] == target, 'Wrong sum'
    return True


def pb_generator(N, upper_bound, lower_bound=0, seed=None):
    """Generate a search sum problem.

    Caveat: this generator does not enforce that there is a single solution.

    Parameters
    ----------
    N : int
        Size of the integer list.
    upper_bound : int
        Upper bound for the generated integer list.
    lower_bound : int, default 0
        Lower bound for the generated integer list.

    Returns
    -------
    (list of int, int)
        A (nums, target) pair.
    """
    if seed is not None:
        random.seed(seed)

    nums = random.choices(range(lower_bound, upper_bound), k=N)
    target = sum(random.choices(nums, k=2))
    return (nums, target)


if __name__ == '__main__':
    from time import time

    nums, target = pb_generator(N=200000, upper_bound=int(1e7), seed=123)

    t_start = time()
    brute_solution = brute_force_search(nums, target)
    t_stop = time()
    check_search_result(*brute_solution, nums, target)
    print(f'Brute-force run: {t_stop - t_start:.3f}s')

    t_start = time()
    sort_solution = smarter_search_sort(nums, target)
    t_stop = time()
    check_search_result(*sort_solution, nums, target)
    print(f'Sort-search run: {t_stop - t_start:.3f}s')
