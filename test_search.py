# -*- coding: utf-8 -*-
"""Tests for the search module."""
import pytest

import search


@pytest.mark.parametrize(
    'nums, target, expected',
    [
        ([2, 7, 11, 15], 9, (0, 1)),
        ([2, 8, 11, 15], 9, None),
        ([8, 7, 11, 2], 9, (3, 1)),
        ([-8, 7, 11, 2], -6, (3, 0)),
    ]
)
def test_brute_force_search(nums, target, expected):
    solution = search.brute_force_search(nums, target)
    assert ((expected is None and solution is None)
            or (set(solution) == set(expected)))


@pytest.mark.parametrize(
    'nums, target, expected',
    [
        ([2, 7, 11, 15], 9, (0, 1)),
        ([2, 8, 11, 15], 9, None),
        ([8, 7, 11, 2], 9, (3, 1)),
        ([-8, 7, 11, 2], -6, (3, 0)),
    ]
)
def test_smarter_search_sort(nums, target, expected):
    solution = search.smarter_search_sort(nums, target)
    assert ((expected is None and solution is None)
            or (set(solution) == set(expected)))


def test_check_search_result_pass():
    assert search.check_search_result(1, 3, [8, 7, 11, 2], 9)
    assert search.check_search_result(3, 1, [8, 7, 11, 2], 9)


def test_check_search_result_same_index():
    with pytest.raises(AssertionError) as excinfo:
        search.check_search_result(1, 1, [1, 2, 3], 4)
    assert 'Same index' in str(excinfo.value)


def test_check_search_result_wrong_sum():
    with pytest.raises(AssertionError) as excinfo:
        search.check_search_result(0, 1, [1, 2, 3], 4)
    assert 'Wrong sum' in str(excinfo.value)
