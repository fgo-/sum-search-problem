# Requirements

* `Python >= 3.6`
* `pytest`

# Run

```bash
$ python search.py 
Brute-force run: 6.330s
Sort-search run: 0.091s
```

# Test

```bash
$ pytest
```